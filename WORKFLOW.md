## Branch naming

Git branch name __must__ meet the following requirements:
- consist of __English words__;  
    👍 `fix-tests-failure`  
    🚫 `fix-defectum-probat`
- use __only dashes to separate words__;  
    👍 `fix-tests-failure`  
    🚫 `fix_tests_failure`
- use __imperative mood for verbs__;  
    👍 `fix-tests-failure`  
    🚫 `fixes-tests-failure`
- __start with the issue number__ when branch is related to some issue (but __DO NOT use MR numbers__);  
    👍 `23-fix-tests-failure`  
    🚫 `fix-tests-failure`
- __reflect the meaning of branch changes__, not the initial problem.    
    👍 `23-fix-tests-failure`  
    🚫 `23-problem-with-failing-tests`

#### Tag naming

Version tag directly __depends on a project versioning scheme__ (usually Semantic Versioning 2.0.0) and __must be the project release version__.  
👍 `2.3.1`, `v5.3.11-alpha.3`  
🚫 `2.3.1-release`, `release-5.3.11-alpha.3`

__For GitLab__ repositories version tag __must start with `v` symbol__ to match `v*` protection mask.  
👍 `v1.2.4`, `v2.3.1-beta.4`  
🚫 `1.2.4`, `2.3.1-beta.4`

Any other __non-version tags__ may be named fluently, but __must follow the branch naming rules__.


### Commits

Every __commit message must contain a short description__ of its changes that meet the following requirements:
- be __on English__ (no other language is allowed);
- __start with a capital letter__;
- has __no punctuation symbols at the end__ (like `;`, `:` or `.`);
- use __imperative mood for verbs__ (as if you are commanding someone: `Fix`, `Add`, `Change` instead of `Fixes`, `Added`, `Changing`);
- use __marked list for multiple changes__, prepended by __one summary line__ and __one blank line__, where each __list item__ must:
    - __start with a lowercase letter__;
    - has __no punctuation symbols at the end__.

##### 👍 Single-line commit message example

```
Update Employee salary algorithm
```

##### 👍 Multiple-line commit message example

```
Implement employees salary and ajax queries

- update Employee salary algorithm
- remove unused files from public/images/ dir
- implement ajax queries for /settings page
```

##### 🚫 Wrong commit message examples

- Summary line starts with a lowercase letter:

    ```
    update Employee salary algorithm
    ```

- Verb is not in the imperative mood:

    ```
    Updates Employee salary algorithm
    ```

- Unnecessary punctuation is present:

    ```
    Update Employee salary algorithm.
    ```

    ```
    Implement employees salary and ajax queries:
    
    - update Employee salary algorithm;
    - remove unused files from public/images/ dir.
    ```

- Missing blank line between the summary line and the marked list:

    ```
    Implement employees salary and ajax queries
    - update Employee salary algorithm
    - remove unused files from public/images/ dir
    ```

- Marked list is indented:

    ```
    Implement employees salary and ajax queries
    
      - update Employee salary algorithm
      - remove unused files from public/images/ dir
    ```

- Marked list items start with a capital letter:

    ```
    Implement employees salary and ajax queries
    
    - Update Employee salary algorithm
    - Remove unused files from public/images/ dir
    ```

## FCM (final commit message)

FCM (final commit message) is a commit message of a merge commit to a mainline branch.

As it will be saved in a repository history forever, it has __extra requirements__ that __must__ be met:
- __contain references__ to related MR and issue (or milestone) __in its summary line__;
- do __not contain__ any non-relative helper markers (like `[skip ci]`);
- __be approved by a lead__ on ~review __before committed and pushed__.

__Common commit messages__ which are not FCM __must NOT contain any references__, because references create crosslinks in mentioned issues/MRs, which leads to spamming issues/MRs with unnecessary information. Only saved in history forever commits are allowed to create such crosslinks.

If а MR contains some __side changes__ which are not directly relevant to the task, then such changes __must be described as a marked list in the `Additionally:` section (separated by a blank line)__ of a FCM.

If FCM describes __project version update__ then it __must reference the milestone__ of this version.

#### 👍 FCM examples

```
Implement employees salary and ajax queries (!43, #54)

- update Employee salary algorithm
- remove unused files from public/images/ dir

Additionally:
- update Git ignoring rules for TOML files
```

```
Bump up project version to 1.54.1 (%71)
```

##### 🚫 Wrong FCM examples

- Bad formatting of references:

    ```
    Implement employees salary and ajax queries(!43,#54)

    - update Employee salary algorithm
    - remove unused files from public/images/ dir
    ```

- Side changes are not separated:

    ```
    Implement employees salary and ajax queries (!43, #54)

    - update Employee salary algorithm
    - remove unused files from public/images/ dir
    - update Git ignoring rules for TOML files
    ```

- Bad formatting of side changes:

    ```
    Implement employees salary and ajax queries (!43, #54)

    - update Employee salary algorithm
    - remove unused files from public/images/ dir
    Additionally:
    - update Git ignoring rules for TOML files
    ```


## Merging

__All merges to the mainline__ project version (`master` branch) __must have an individual MR (merge request)__ and must be __done only in fast-forward manner__. This is required to keep mainline history linear, simple and clear.

__The only exception__ is __merging two mainline branches__, because we want to preserve histories of both mainline branches (one of them is usually a temporary mainline required for implementing multiple steps separately from a base mainline).

To achieve fast-forward merge, __all branch commits__ (which doesn't exist in mainline) __must be squashed and rebased onto the latest mainline commit__. Notable moments are:
- Before rebase __do not forget to merge your branch with latest mainline branch updates__, otherwise rebase result can broke changes.
- To perform rebase and squash safely, and not to lose any changes, the __`git merge --squash` must be used__. It's proven and stable way to squash changes, while `git rebase` can be unexpectedly messy with merge commits.
- __After rebase__ you should __use `git push --force` to correctly update your remote branch__.

__Use `Merge` (or `Merge when pipeline succeeds`) button in GitLab__ for merging, as far as it performs fast-forward merges and removes a necessity to wait until CI pipeline is finished.

#### Squash merging steps

##### Using GitLab/GitHub UI

Performing squash merge correctly can be quite tricky when doing manually. To avoid complexity and mistakes in a day-to-day routine the GitLab UI squash merging is the __most preferred way__ for merging and a __developer should use it whenever it's possible__.

0. Merge with latest mainline branch.

1. Check the `Squash commits` and `Delete source branch` checkboxes near the `Merge` (or `Merge when pipeline succeeds`) button.

2. Click the appeared `Modify commit message` link below.

3. Paste the FCM into the appeared `Squash commit message` field.

4. Click the `Merge` (or `Merge when pipeline succeeds`) button.

__For GitHub__ projects:
- __First line__ of FCM must go __as a title__ of the squash commit and __everything after as a message__.

Squash merging via GitLab/GitHub UI also preserves the whole branch commits history in the MR (or PR), which is good for history purposes.


## Workflow

The basic workflow of one task development is described with the diagram below:
```markdown
 |
 | Report issue #32
 | and assign it to Maintainer or Lead
 | (DO use available issue templates)
 V
+--------------------+
| Issue #32 reported |
+--------------------+
 |
 | Maintainer or Lead applies correct labels to issue #32
 |
 | Maintainer or Lead attaches issue #32 to %10 milestone
 |
 | Maintainer or Lead reassigns issue #32 to Developer
 V
+---------------------------------+
| Issue #32 assigned to Developer |
+---------------------------------+
 |
 | Developer investigates issue #32,
 | proposes and describes possible solutions
 |
 | Lead discusses solutions with Developer (and Maintainer, optionally)
 | and approves some solution
 |
 | Developer creates branch `32-relevant-name` from latest mainline branch
 | and creates MR (merge request) `32-relevant-name` -> mainline branch:
 | - name of MR must start with `Draft: ` prefix
 |   and contain ` (#32)` reference
 | - description of MR must mention #32 issue
 | (DO use available MR templates)
 |
 | Developer assigns MR !57 to himself
 |
 | Developer attaches MR !57 to %10 milestone
 |
 | Developer applies correct labels to MR !57
 V
+--------------------------+
| MR !57 on implementation |<---------------------------------+
+--------------------------+                                  |
 |                                                            |
 | Developer removes ~reopened label from MR !57 (if any)     |
 |                                                            |
 | Developer pulls `32-relevant-name` branch from remote      |
 | to consider any changes                                    |
 |                                                            |
 | Developer implements MR !57 changes                        |
 |                                                            |
 | Developer ensures that:                                    |
 | - all business requirements are implemented                |
 | - all changes follow project's code style                  |
 | - all project tests and builds pass successfully           |
 |                                                            |
 | Developer updates CHANGELOG                                |
 | with MR !57 changes (if required)                          |
 |                                                            |
 | Developer updates "Deployment" section of CHANGELOG        |
 | with manual deployment actions (if required)               |
 |                                                            |
 | Developer performs final detailed self-review              |
 | of the whole changes made in MR !57                        |
 | to ensure that quality requirements are met                |
 |                                                            |
 | Developer prepares FCM (final commit message)              |
 | and posts it as discussion                                 |
 |                                                            |
 | Developer adds ~review label to MR !57                     |
 |                                                            |
 | Developer assigns Lead as reviewer of MR !57               |
 | (DO use "Reviewers" MR field on GitLab)                    |
 V                                                            |
+------------------+                                          |
| MR !57 on review |                                          |
+------------------+                                          |
   |                                                          |
   | Lead reviews changes of MR !57                           |
   | and comments them in MR !57 as separate discussions      |
   |                                                          |
   | Lead ensures that CHANGELOG is updated (if required)     |
   |                                                          |
   | Lead ensures that "Deployment" section of CHANGELOG      |
   | is present and correct (if required)                     |
   |                                                          |
   | Lead describes review result in MR !57                   |
   | as a separate discussion                                 |
   |                                                          |
   | Lead removes ~review label from MR !57                   |
  / \                                                         |
 |   |                                                        |
 |   | Lead does not approve MR !57                           |
 |   |                                                        |
 |   | Lead adds ~reopened label to MR !75                    |
 |   |                                                        |
 |   | Lead removes himself from MR !75 reviewers             |
 |   | (DO use "Reviewers" MR field on GitLab)                |
 |   |                                                        |
 |   | Lead notifies Developer about finished review of       |
 |   | MR !57 via separate comment by mentioning him          |
 |   V                                                        |
 |  +-----------------+                                       |
 |  | MR !57 reopened |                                       |
 |  +-----------------+                                       |
 |   |                                                        |
 |   | Developer considers review result                      |
 |   |                                                        |
 |   +--------------------------------------------------------+
 |
 | Lead approves (with reaction) posted FCM in MR !57
 | or posts correct FCM instead
 |
 | Lead approves MR !57 (DO use "Approve" button on GitLab)
 |
 | Lead adds ~LGTM label (look good to me) to MR !57
 |
 | Lead notifies Developer about finished review of MR !57
 | via separate comment by mentioning Developer
 |
 | Lead initiates "Retrospective" discussion by mentioning Developer
 | in MR !57 comments (if Lead needs it)
 V
+-----------------+
| MR !57 approved |
+-----------------+
 |
 | Developer considers review result
 |
 | Developer pulls `32-relevant-name` branch from remote
 | to consider any changes
 |
 | Developer prepares MR !57 to be merged into mainline branch
 |
 | Developer merges latest mainline into branch `32-relevant-name`
 |
 | Developer ensures that MR !57 name and description are correct,
 | verbose enough and up-to-date. 
 |
 | Developer removes `Draft: ` prefix from MR !57 name
 |
 | Developer removes ~LGTM label from MR !57
 |
 | Developer removes any other temporary labels from MR !57
 |
 | Developer ensures that MR !57 is attached to correct milestone
 |
 | Developer resolves all discussions in MR !57
 |
 | Developer merges squashed & rebased branch `32-relevant-name` into mainline
 | with fast-forward merge
 | (DO use "Merge"/"Merge when pipeline succeeds" button on GitLab
 | with "Squash commits" checkbox and copy-paste approved FCM as a commit message)
 |
 | Developer removes `32-relevant-name` remote branch
 | (DO use "Remove source branch" checkbox or button on GitLab)
 V
+---------------+
| MR !57 merged |
+---------------+
 |
 | Developer removes any temporary labels from issue #32
 |
 | Developer ensures that issue #32 is attached to correct milestone
 |
 | Developer closes issue #32
 |
 | Developer initiates "Retrospective" discussion by mentioning Lead
 | in MR !57 comments (if Developer needs it)
 V
+--------------------+
| Issue #32 resolved |
+--------------------+
```

If an issue is reopened lately, then its lifecycle begins from scratch, but with a different MR.

When there is no need to create an issue, just create a MR directly and repeat the workflow above without issue-related parts.

While a MR is __on ~review a developer cannot push any changes__ unless explicitly coordinated with his lead.


### Multiple reviews

Sometimes there is a need for a MR to be ~"review"ed by multiple persons (by lead and maintainer, for example). In such case ~"review"s are performed in a cascade manner.

```markdown
+--------------------------+
| MR !57 on implementation |<---------------------------------+
+--------------------------+                                  |
 |                                                            |
 | Developer removes ~reopened label from MR !57 (if any)     |
 |                                                            |
 | Developer assigns Lead as reviewer of MR !57               |
 | and applies ~review label                                  ^
 V                                                            |
+--------------------------+                                  |
| MR !57 on review by Lead |                                  |
+--------------------------+                                  |
   |                                                          |
   | Lead reviews changes of MR !57                           |
   | and comments them in MR !57 as separate discussions      ^
   | by mentioning Developer                                  |
   |                                                          |
  / \                                                         |
 |   | Lead does not approve MR !57                           |
 |   |                                                        |
 |   | Lead removes ~review label from MR !57,                |
 |   | removes himself from MR !57 reviewers,                 |
 |   | and adds ~reopened label                               |
 |   |                                                        ^
 |   | Lead notifies Developer about finished review of       |
 |   | MR !57 via separate comment by mentioning him          |
 |   V                                                        |
 |  +-----------------+                                       |
 |  | MR !57 reopened |                                       |
 |  +-----------------+                                       |
 |   ^   |                                                    |
 |   |   | Developer considers review result                  ^
 |   |   |                                                    |
 |   |   +-------------->-------------------->----------------+
 |   +------------------<--------------------<----------------+
 |                                                            |
 | Lead approves MR !57 changes                               |
 |                                                            |
 | Lead assigns Reviewer2 as reviewer of MR !57               |
 V                                                            ^
+-------------------------------+                             |
| MR !57 on review by Reviewer2 |                             |
+-------------------------------+                             |
   |                                                          |
   | Reviewer2 reviews changes of MR !57                      |
   | and comments them in MR !57 as separate discussions      |
   | by mentioning Developer and Lead                         ^
   |                                                          |
  / \                                                         |
 |   | Reviewer2 does not approve MR !57                      |
 |   |                                                        |
 |   | Reviewer2 removes ~review label from MR !57,           |
 |   | removes himself from MR !57 reviewers,                 |
 |   | and adds ~reopened label                               |
 |   |                                                        ^
 |   | Reviewer2 removes Lead from MR !57 reviewers           |
 |   | (if decides so)                                        |
 |   |                                                        |
 |   | Reviewer2 notifies Developer and Lead                  |
 |   | about finished review of MR !57                        |
 |   | via separate comment by mentioning them                |
 |   V                                                        |
 |  +-----------------+                                       ^
 |  | MR !57 reopened |                                       |
 |  +-----------------+                                       |
 |   ^   |                                                    |
 |   |   | Lead considers review result                       |
 |   |   |                                                    |
 |   |   +-------------->-------------------->----------------+
 |   +------------------<--------------------<----------------+
 |                                                            |
 | Reviewer2 approves MR !57 changes                          |
 |                                                            |
 | Reviewer2 assigns Reviewer3 as reviewer of MR !57          |
 V                                                            ^
+-------------------------------+                             |
| MR !57 on review by Reviewer3 |                             |
+-------------------------------+                             |
   |                                                          |
   | Reviewer3 reviews changes of MR !57                      |
   | and comments them in MR !57 as separate discussions      |
   | by mentioning Developer, Lead and Reviewer2              ^
   |                                                          |
  / \                                                         |
 |   | Reviewer3 does not approve MR !57                      |
 |   |                                                        |
 |   | Reviewer3 removes ~review label from MR !57,           |
 |   | removes himself from MR !57 reviewers,                 |
 |   | and adds ~reopened label                               |
 |   |                                                        ^
 |   | Reviewer3 removes Reviewer2 from MR !57 reviewers      |
 |   | (if decides so)                                        |
 |   |                                                        |
 |   | Reviewer3 removes Lead from MR !57 reviewers           |
 |   | (if decides so)                                        |
 |   |                                                        |
 |   | Reviewer3 notifies Developer, Lead and Reviewer2       |
 |   | about finished review of MR !57                        ^
 |   | via separate comment by mentioning them                |
 |   V                                                        |
 |  +-----------------+                                       |
 |  | MR !57 reopened |                                       |
 |  +-----------------+                                       |
 |   |                                                        |
 |   | Reviewer2 considers review result                      ^
 |   |                                                        |
 |   +------------------>-------------------->----------------+
 |
 | Reviewer3 approves MR !57 changes
 |
 | Reviewer3 adds ~LGTM label (look good to me) to MR !57
 |
 | Reviewer3 notifies Developer, Lead and Reviewer2
 | about finished review of MR !57
 | via separate comment by mentioning them
 V
+-----------------+
| MR !57 approved |
+-----------------+
 |
 | Developer, Lead and Reviewer2 consider review result
 V
 ...
```



[p:squash-steps]: #squash-merging-steps
[p:workflow]: #workflow
[p:merging]: #merging
[p:fcm]: #fcm-final-commit-message
