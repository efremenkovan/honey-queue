Все rout'ы начинаются с /api/v1/

Auth
  - Регистрация пользователя (пока только владелец ресторана, там разбиение на группы по правам не предусмотрено)
    - /auth/users/
    - POST
    - { login: string, password: string, email: string }
    - Вернется { email: string, login: string, id: number }
    - пока без подтверждения по мылу, не прикрутил host

  - Вход
    - /auth/jwt/verify/
    - POST
    - { login: string, password: string }
    - { access: "TOKEN", refresh: "TOKEN" } (access token - использовать в header-ах "Authorization": "Bearer <TOKEN>", refresh - чтоб восстановить закончившуюся сессию)


Restaurants
  - создание
      - /restaurants/
      - POST
      - { name: string, description: string, avatar: file, phone: 11 чисел, webstie?: string, city: string, address: string, booking_time_diff: int }
      - Вернется то же самое + id, owner и created_at

  - список всех
    - /restaurants/
    - GET
    - 
    - Restaurant[]

  - детали 1 рестика
    - GET
    - /restaurants/restaurant/<id>/
    -
    - Restaurant

  - все рестораны, связанные с аккаунтом
    - GET
    - /restaurants/<id>/
    -
    - Restaurant[]

Расписание
  - Cоздание дня
    - POST
    - /restaurants/restaurant/<id>/schedule/create
    - { restaurant_id: number, weekday: number, time_from: unix time, time_to: unix time} 
    - такой же объект

  - Редактирование
    - PATCH
    - /restaurants/restaurant/<id>/schedule/update/
    - { restaurant_id: number, weekday: number, time_from: unix time, time_to: unix time} 
    - такой же объект c обновленными данными

  - Получание
    - Get
    - /restaurants/restaurant/<id>/schedule/
    -
    - Array<{ restaurant_id: number, weekday: number, time_from: unix time, time_to: unix time}>

Столики
  - Все столики ресторана
    - GET
    - /tables/of-restaurant/<id>/(?around=<unix_time>)
    - 
    - { restaurant_id: string, max_people: number, number: number, is_taken }

  - Создание столика
    - POST
    - /tables/
    - { restaurant_id: string, max_people: number, number: number }
    - Вернется То же самое + id

  - Удаление столика
    - DELETE
    - /tables/<id>/
    -
    - 204\403\401

  - Ручной контроль занятости столика
    - TBD

  - Проверка занятости столика на диапазоне времени
    - TBD

Booking
  - Создание
    - /booking/create/
    - POST
    - { restaurant_id: number, phone_number: number, name: string, table_id?: string, person_amount: number, date_time: number}
    - то же самое + id слолика + id брони

  - Отмена брони
    - /booking/cancel/<id>/
    - POST
    - 
    - 204

  - закрытие брони (если человек ушел - делает админ)
    - /booking/close/<id>/
    - POST
    -
    - 204

  - история броней ресторана
    - /booking/history/of-restaurant/<id>/
    - GET
    -
    - Array<{ restaurant_id: number, phone_number: number, name: string, table_id: string, person_amount: number, date_time: number, is_canceled: bool }>