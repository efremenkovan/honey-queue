comma := ,

# Checks two given strings for equality.
eq = $(if $(or $(1),$(2)),$(and $(findstring $(1),$(2)),\
                                $(findstring $(2),$(1))),1)

down: docker.down

up: docker.up

serve: npm.serve

build: docker.build

docker.down:
	docker-compose down --rmi=local -v

docker.build:
	docker-compose build

docker.up: docker.down
ifeq ($(rebuild),yes)
	@make docker.build
endif
	docker-compose up \
		$(if $(call eq,$(background),yes),-d,--abort-on-container-exit)

ifeq ($(background),yes)
ifeq ($(log),yes)
	@docker logs -f socfront-app
endif
endif

npm.build:
	cd client && npm run build

npm.serve:
	make docker.up background=yes log=no
	cd ./client && npm run serve
