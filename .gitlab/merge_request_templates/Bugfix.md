Resolves <paste issue reference>




## Synopsis

<Give a brief overview of the problem>




## Solution

<Describe how exactly the problem is (or will be) resolved>




## Checklist

- Created MR:
    - [ ] Name contains `Draft: ` prefix
    - [ ] Name contains issue reference
    - [ ] Has `kind: ` labels applied
- [ ] Documentation is updated (if required)
- [ ] Tests are updated (if required)
- [ ] Changes conform [code style][c:1]
- [ ] [CHANGELOG entry][c:2] is added (if required)
- [ ] [FCM][c:3] is posted
    - [ ] and approved
- [ ] ~review is completed and changes are approved
- Before merge:
    - [ ] Commits are [squashed and rebased][c:4]
    - [ ] Milestone is set
    - [ ] MR's name and description are correct and up-to-date
    - [ ] `Draft: ` prefix is removed
    - [ ] All temporary labels are removed





[c:1]: https://gitlab.com/efremenkovan/honey-queue/-/blob/master/CODESTYLE.md
[c:2]: https://gitlab.com/efremenkovan/honey-queue/-/blob/master/CHANGELOG.md
[c:3]: https://gitlab.com/efremenkovan/honey-queue/-/blob/master/WORKFLOW.md#fcm
[c:4]: https://gitlab.com/efremenkovan/honey-queue/-/blob/master/WORKFLOW.md#squash-merging-steps




/assign me
/label ~"type: enhancement"
/label ~"kind: documentation"
