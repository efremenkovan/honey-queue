Resolves <paste issue reference>  
<and/or>  
Required for <paste issue/MR references>  
<and/or>  
Related to <paste issues/MRs references>  

<Remove the lines above if there are no related issues/MRs>




## Summary

<Summarize the meaning and the purpose of this roadmap>




## Roadmap

<Describe the roadmap as a numbered task list, where each step should contain references to its MR and checked after its completion>

- [ ] 1. First step. (<paste MR reference>)




## Checklist

- Created MR:
    - [ ] Name contains `Draft: ` prefix
    - [ ] Name contains issue reference (if required)
    - [ ] Has `type: ` and `kind: ` labels applied
- [ ] ~review is completed and changes are approved
- Before merge:
    - [ ] Milestone is set
    - [ ] MR's name and description are correct and up-to-date
    - [ ] `Draft: ` prefix is removed
    - [ ] All temporary labels are removed




/assign me
/label ~"type: roadmap"
