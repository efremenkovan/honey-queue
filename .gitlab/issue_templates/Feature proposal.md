Revealed from <paste issues/MRs references>  
<and/or>  
Related to <paste issues/MRs references>  

<Remove the lines above if there are no related issues/MRs>




## Problem to solve

<Describe the problem that feature is going to solve>




## Proposal

<Give a detailed proposal of problem solving, describe its pros and cons>




## Prior art

<Provide a link and brief info about attempts and proposals which tried to solve this problem before, or are quite relative to this proposal>
<If there is nothing to refer - just remove this section completely>




## Alternatives

<Provide a brief description of alternative solutions, describe their pros and cons>




## Links & references

<Provide links and references that are related to this proposal and problem>
<If there is nothing to refer - just remove this section completely>




/label ~"type: feature"
