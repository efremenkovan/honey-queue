Part of <paste issue/MR reference>  
<and/or>  
Revealed from <paste issue/MR reference>  
<and/or>  
Required for <paste issues/MRs references>  
<and/or>  
Requires <paste issues/MRs references>  
<and/or>  
Related to <paste issues/MRs references>  

<Remove the lines above if there are no related issues/MRs>




## Background

<Describe the preconditions and the situation which lead to the problem>




## Problem to solve

<Describe the problem to be solved by this task>




## Possible solutions

<Describe possible solutions and assumptions about them>
<If there is nothing to propose - just remove this section completely>
