Revealed from <paste issue/MR reference>  
<and/or>  
Caused by <paste issues/MRs references>  
<and/or>  
Related to <paste issues/MRs references>  

<Remove the lines above if there are no related issues/MRs>




## Summary

<Summarize the bug encountered concisely>




## Steps to reproduce

<How one can reproduce the issue - this is very important>




## What is the current _bug_ behavior?

<What actually happens>




## What is the expected _correct_ behavior?

<What you should see instead>




## Relevant logs and/or screenshots

<Paste any relevant logs - please use code blocks (`````) to format console output, logs, and code as it's very hard to read otherwise>
<If there is no logs/screenshots - just remove this section completely>



## Possible fixes

<If you can, link to the line of code that might be responsible for the problem>
<If there is nothing to propose - just remove this section completely>




/label ~"type: bug"
