const path = require("path");

module.exports = {
  stories: [
    'storybook.stories.js',
  ],
  addons: [
    "@storybook/addon-links",
    "@storybook/addon-essentials",
    "@storybook/preset-scss"
  ],
  webpackFinal: async (config, { configType }) => {
    config.module.rules.push(
      {
        test: /\.pug$/,
        use: [
          { loader: 'pug-plain-loader' }
        ]
      }
    );
    config.resolve.alias = {
      ...config.resolve.alias,
      "~/": path.resolve(__dirname, "..", "/src/"),
      "@/": path.resolve(__dirname, "..", "/src/"),
    };
    return config;
  },
}
