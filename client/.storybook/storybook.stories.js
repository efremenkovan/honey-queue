import { storiesOf } from '@storybook/vue3';
import get from 'lodash/get';

const stories = require.context('../src/components', true, /\.story\.vue$/);

stories.keys().forEach((story) => {
    try {
        const component = stories(story).default;
        const category = get(component, 'story.category', 'components');
        const name = get(component, 'story.name', component.name) || story;

        storiesOf(category, module).add(name, () => component);
    } catch (e) {
        console.log(e)
        storiesOf('errors', module).add(story, () => ({
            template: `<div>${e.toString()}</div>`
        }));
    }
});
