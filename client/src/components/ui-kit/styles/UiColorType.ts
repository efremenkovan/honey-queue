enum UiColorType {
  Primary = 'primary',
  Accent = 'accent',
  Done = 'done',
  Error = 'error',
}

export default UiColorType;
