enum UiSize {
  Small = 'small',
  Normal = 'normal',
  Medium = 'medium',
  Big = 'big',
}

export default UiSize;
