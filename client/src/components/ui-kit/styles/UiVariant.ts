enum UiVariant {
  Default = 'default',
  Outlined = 'outlined',
  Decorated = 'decorated'
}

export default UiVariant;
