from rest_framework import routers
from django.urls import path, include, re_path

from . import views


urlpatterns = [
    re_path(r'create/?', views.RestaurantCreateView.as_view()),
    path('', views.RestaurantsListView.as_view()),
    re_path(r'restaurant/<int:pk>/?', views.RestaurantDetailsView.as_view()),
    re_path(r'restaurant/<int:pk>/update/?', views.RestaurantUpdateView.as_view()),
    re_path(r'<int:pk>/?', views.RestaurantsOfUserView.as_view()),
    re_path(r'restaurant/<int:pk>/schedule/create/?', views.RestaurantScheduleCreateView.as_view()),
    re_path(r'restaurant/<int:pk>/schedule/update/?', views.RestaurantScheduleUpdateView.as_view()),
    re_path(r'restaurant/<int:pk>/schedule/?', views.RestaurantScheduleView.as_view()),
]
