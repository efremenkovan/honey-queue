import datetime

from django.core.exceptions import PermissionDenied
from rest_framework import serializers, status
from rest_framework.response import Response

from .models import Restaurant, Schedule


class RestaurantSerializer(serializers.ModelSerializer):
    class Meta:
        model = Restaurant
        fields = '__all__'

    def create(self, validated_data):
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            rest = Restaurant(
                name = validated_data.get("name"),
                address = validated_data.get("address"),
                description = validated_data.get("description"),
                avatar = validated_data.get("avatar"),
                phone = validated_data.get("phone"),
                city = validated_data.get("city"),
                booking_time_diff = validated_data.get("booking_time_diff"),
                website = validated_data.get("website"),
                created_at = round(datetime.datetime.now().timestamp() * 1000),
                owner = request.user,
            )
            rest.save()
            return rest
        else:
            raise PermissionDenied({ 'content': 'Authentication required' })

    def update(self, instance, validated_data):
        request = self.context.get("request")
        if request and hasattr(request, "user") and request.user.id == instance.owner_id:
            instance.name = validated_data.get("name", instance.name)
            instance.address = validated_data.get("address", instance.address)
            instance.description = validated_data.get("description", instance.description)
            instance.avatar = validated_data.get("avatar", instance.avatar)
            instance.phone = validated_data.get("phone", instance.phone)
            instance.city = validated_data.get("city", instance.city)
            instance.booking_time_diff = validated_data.get("booking_time_diff", instance.booking_time_diff)
            instance.website = validated_data.get("website", instance.website)
            instance.save()
            return instance
        else:
            raise PermissionDenied({'content': 'Authentication required'})


class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = '__all__'

    def create(self, validated_data):
        request = self.context.get("request")
        restaurant_owner_id = RestaurantSerializer(validated_data.get("restaurant_id")).data.get("owner")

        if request and hasattr(request, "user") and request.user.id == restaurant_owner_id:
            schedule = Schedule(**validated_data)
            schedule.save()
            return schedule
        else:
            return Response({ 'content': 'Forbidden' }, status=status.HTTP_403_FORBIDDEN)


class ScheduleUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = '__all__'

    def create(self, validated_data):
        request = self.context.get("request")
        restaurant_owner_id = RestaurantSerializer(validated_data.get("restaurant_id")).data.get("owner")

        if request and hasattr(request, "user") and request.user.id == restaurant_owner_id:
            try:
                schedule = Schedule.objects.get(id = validated_data.get('restaurant_id'), weekday = validated_data.get('weekday'))
            except Schedule.DoesNotExist:
                raise Http404

            schedule.time_from = validated_data.get("time_from")
            schedule.time_to = validated_data.get("time_to")
            schedule.update()
            return schedule
        else:
            return Response({ 'content': 'Forbidden' }, status=status.HTTP_403_FORBIDDEN)