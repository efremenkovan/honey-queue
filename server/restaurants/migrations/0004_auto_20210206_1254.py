# Generated by Django 3.1 on 2021-02-06 12:54

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('restaurants', '0003_restaurant_created_at'),
    ]

    operations = [
        migrations.AddField(
            model_name='restaurant',
            name='owner',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='restaurants', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='restaurant',
            name='booking_time_diff',
            field=models.BigIntegerField(),
        ),
        migrations.AlterField(
            model_name='restaurant',
            name='created_at',
            field=models.BigIntegerField(default=1612616117908),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='time_from',
            field=models.BigIntegerField(),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='time_to',
            field=models.BigIntegerField(),
        ),
        migrations.AlterField(
            model_name='schedule',
            name='weekday',
            field=models.IntegerField(unique=True),
        ),
    ]
