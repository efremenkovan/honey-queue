import json

from django.http import Http404
from django.shortcuts import render
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly

from . import serializers
from .models import Restaurant, Schedule
from .permissions import IsOwner, IsOwnerOrReadonly


class RestaurantCreateView(generics.CreateAPIView):
    serializer_class = serializers.RestaurantSerializer


class RestaurantDetailsView(generics.RetrieveDestroyAPIView):
    serializer_class = serializers.RestaurantSerializer
    permission_classes = [IsAuthenticatedOrReadOnly, IsOwnerOrReadonly]
    queryset = Restaurant.objects.all()

    def get(self, request, pk):
        try:
            restaurant = Restaurant.objects.get(id = pk)
        except Restaurant.DoesNotExist:
            raise Http404

        self.check_object_permissions(self.request, restaurant)
        return Response(serializers.RestaurantSerializer(restaurant).data)


class RestaurantsListView(generics.ListAPIView):
    serializer_class = serializers.RestaurantSerializer
    queryset = Restaurant.objects.all()


class RestaurantsOfUserView(generics.ListAPIView):
    serializer_class = serializers.RestaurantSerializer
    queryset = Restaurant.objects.all()

    def get(self, request, pk):
        if pk != request.user.id:
            return Response({'content': 'Forbidden'}, status=status.HTTP_403_FORBIDDEN)
        restaurants = Restaurant.objects.filter(owner_id = pk)
        return Response([serializers.RestaurantSerializer(restaurant).data for restaurant in restaurants])


class RestaurantTablesView(generics.ListAPIView):
    serializer_class = serializers.RestaurantSerializer
    queryset = Restaurant.objects.all()

    def get(self, request, pk):
        restaurants = Restaurant.objects.filter(owner_id=pk)
        return Response([serializers.RestaurantSerializer(restaurant).data for restaurant in restaurants])


class RestaurantScheduleCreateView(generics.CreateAPIView):
    serializer_class = serializers.ScheduleSerializer
    queryset = Schedule.objects.all()


class RestaurantScheduleUpdateView(generics.UpdateAPIView):
    serializer_class = serializers.ScheduleUpdateSerializer
    queryset = Schedule.objects.all()


class RestaurantUpdateView(generics.UpdateAPIView):
    serializer_class = serializers.RestaurantSerializer
    queryset = Restaurant.objects.all()


class RestaurantScheduleView(generics.ListAPIView):
    serializer_class = serializers.ScheduleSerializer
    queryset = Schedule.objects.all()

    def get(self, request, pk):
        return Response({
            'schedule': [
                serializers.ScheduleSerializer(day).data for day in Schedule.objects.filter(restaurant_id = pk)
            ]
        })