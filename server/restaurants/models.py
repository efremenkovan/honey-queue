import datetime

from django_unixdatetimefield import UnixDateTimeField
from django.db import models


# Restaurant object model.
class Restaurant(models.Model):
    name = models.CharField(max_length = 255)
    description = models.CharField(max_length = 500)
    avatar = models.ImageField()
    phone = models.CharField(max_length = 11)
    website = models.CharField(max_length = 255, null = True)
    city = models.CharField(max_length = 255)
    address = models.CharField(max_length = 255)
    booking_time_diff = models.BigIntegerField()
    created_at = models.BigIntegerField(default = round(datetime.datetime.now().timestamp() * 1000))
    owner = models.ForeignKey('users.RestaurantOwner', related_name='restaurants', on_delete=models.CASCADE, default=None, blank=True, null=True)

    @staticmethod
    def get_all_restaurants():
        return Restaurant.objects.all()

    @staticmethod
    def get_restaurant_by_id(id: str, with_schedule: bool = False):
        rest = Restaurant.objects.get(id=id)
        if with_schedule == True:
            rest['schedule'] = rest.get_schedule()

        return rest

    def get_schedule(self):
        return Schedule.objects.filter(restaurant_id = id),

    @staticmethod
    def patch_restaurant(
        id: str,
        name: str,
        address: str,
        description: str,
        avatar: str,
        phone: str,
        city: str,
        booking_time_diff: str,
        website: str = None,
    ):
        rest: Restaurant = Restaurant.objects.get(id=id)
        rest.name = name
        rest.address = address
        rest.description = description
        rest.avatar = avatar
        rest.phone = phone
        rest.city = city
        rest.booking_time_diff = booking_time_diff
        rest.website = website
        rest.save()
        return rest
        
    @staticmethod
    def delete_restaurant_by_id(id: str):
        rest: Restaurant = Restaurant.objects.get(id=id)
        rest.delete()


class Schedule(models.Model):
    restaurant_id = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    weekday = models.IntegerField(unique = True)
    time_from = models.BigIntegerField()
    time_to = models.BigIntegerField()

    @staticmethod
    def create_schedule_day(weekday: int, time_from: int, time_to: int, restaurant_id: int):
        day = Schedule(weekday, time_from, time_to)
        day.save()
        return day

    @staticmethod
    def get_by_restaurant_id(id: int):
        return Schedule.objects.filter(restaurant_id = id)