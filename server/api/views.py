from rest_framework import generics, status
from rest_framework.response import Response


class PasswordResetConfirmView(generics.RetrieveAPIView):
    def get(self, request):
        return Response({
            'uid': request.GET.get('uid'),
            'token': request.GET.get('token')
        })
