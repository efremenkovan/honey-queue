from django.urls import path, include, re_path

from . import views


urlpatterns = [
    re_path(r'v1/restaurants/?', include('restaurants.urls')),
    path('v1/booking/', include('bookings.urls')),
    path('v1/tables/', include('tables.urls')),
    path('v1/auth/', include('djoser.urls')),
    path('v1/auth/', include("djoser.urls.jwt")),
    path('v1/auth/', include('djoser.urls.authtoken')),
]