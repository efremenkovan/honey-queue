# Generated by Django 3.1 on 2021-02-05 21:10

from django.db import migrations
import users.models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_auto_20210205_2017'),
    ]

    operations = [
        migrations.AlterModelManagers(
            name='restaurantowner',
            managers=[
                ('objects', users.models.RestaurantOwnerManager()),
            ],
        ),
        migrations.RenameField(
            model_name='restaurantowner',
            old_name='is_admin',
            new_name='is_staff',
        ),
    ]
