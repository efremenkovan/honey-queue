from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager


class RestaurantOwnerManager(BaseUserManager):
    use_in_migrations = True

    def create_user(self, email, login, password = None):
        if not email:
            raise ValueError('User must have an email')

        if not login:
            raise ValueError('User must have a login')

        user = self.model(
            email = self.normalize_email(email),
            login = self.model.normalize_username(login),
        )

        user.login = login
        user.set_password(password)
        user.save(using = self._db)
        return user

    def create_superuser(self, email, login, password = None):
        return (email, login, password)
        user = self.create_user(
            email,
            login,
            password = password,
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class RestaurantOwner(AbstractBaseUser):
    email = models.EmailField(max_length = 255, unique = True)
    is_active = models.BooleanField(default = True)
    is_staff = models.BooleanField(default = False)
    login = models.CharField(max_length = 255, unique = True)
    objects = RestaurantOwnerManager()
    USERNAME_FIELD = 'login'
    EMAIL_FIELDS = 'email'
    REQUIRED_FIELDS = ['email']

    def __str(self):
        return self.email