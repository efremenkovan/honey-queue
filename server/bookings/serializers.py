import datetime

from django.core.exceptions import SuspiciousOperation
from rest_framework import serializers, status
from rest_framework.response import Response

from tables.models import Table
from tables.serializers import TableSerializer

from .models import Booking, BookingHistory


## 3 hrs in milliseconds. Should be optional in the future.
EXPECTED_BOOKING_TIME_DIFF = 10_800_000

class BookingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Booking
        exclude = ['created_at']

    def create(self, validated_data):
        booking_time = validated_data.get('date_time')

        if validated_data.get("table_id") == None:
            bookings = Booking.objects.filter(
                date_time__gt = booking_time - EXPECTED_BOOKING_TIME_DIFF,
                date_time__lt = booking_time + EXPECTED_BOOKING_TIME_DIFF,
                restaurant_id = validated_data.get('restaurant_id'),
            )

            all_tables = Table.objects.filter(restaurant_id = validated_data.get('restaurant_id'))

            free_table = None
            for table in all_tables:
                is_taken = False
                for booking in bookings:
                    if BookingSerializer(booking).data.get('table_id') == TableSerializer(table).data.get('id'):
                        is_taken = True
                        break
                if is_taken == False:
                    free_table = table
                    break


            if free_table == None:
                raise SuspiciousOperation({ 'content': 'No available tables' })

            booking = Booking(
                **validated_data,
                table_id = free_table,
                created_at = round(datetime.datetime.now().timestamp() * 1000)
            )
            booking.save()
            return booking
        else:
            bookings = Booking.objects.filter(
                date_time__gt = booking_time - EXPECTED_BOOKING_TIME_DIFF,
                date_time__lt = booking_time + EXPECTED_BOOKING_TIME_DIFF,
                restaurant_id = validated_data.get('restaurant_id'),
                table_id = validated_data.get('table_id'),
            )

            if len(bookings) > 0:
                raise SuspiciousOperation({ 'content': 'Table is taken' })

            booking = Booking(
                **validated_data,
                created_at = round(datetime.datetime.now().timestamp() * 1000)
            )
            booking.save()
            return booking


class BookingHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = BookingHistory
        fields = '__all__'

