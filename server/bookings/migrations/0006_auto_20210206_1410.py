# Generated by Django 3.1 on 2021-02-06 14:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('bookings', '0005_auto_20210206_1409'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='created_at',
            field=models.BigIntegerField(default=1612620618427),
        ),
    ]
