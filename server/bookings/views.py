from django.http import Http404
from django.shortcuts import render
from rest_framework import generics, status
from rest_framework.response import Response

from bookings.serializers import EXPECTED_BOOKING_TIME_DIFF

from . import serializers
from .models import BookingHistory, Booking


class BookingCreateView(generics.CreateAPIView):
    serializer_class = serializers.BookingSerializer
    queryset = Booking.objects.all()


class BookingCancelView(generics.CreateAPIView):
    serializer_class = serializers.BookingSerializer
    queryset = Booking.objects.all()

    def post(self, request, pk):
        try:
            booking = Booking.objects.get(id = pk)
        except Booking.DoesNotExist:
            raise Http404

        booking.cancel()
        return Response({}, status.HTTP_204_NO_CONTENT)


class BookingCloseView(generics.CreateAPIView):
    serializer_class = serializers.BookingSerializer
    queryset = Booking.objects.all()

    def post(self, request, pk):
        try:
            booking = Booking.objects.get(id = pk)
        except Booking.DoesNotExist:
            raise Http404

        booking.close()
        return Response({}, status.HTTP_204_NO_CONTENT)


class BookingsHistoryOfRestaurant(generics.ListAPIView):
    serializer_class = serializers.BookingHistorySerializer
    queryset = BookingHistory.objects.all()

    def get(self, request, pk):
        return Response({ 'booking_history': [
            serializers.BookingHistorySerializer(item).data for item in BookingHistory.objects.filter(restaurant_id = pk)
        ] })
