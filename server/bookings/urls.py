from rest_framework import routers
from django.urls import path, include, re_path

from . import views


urlpatterns = [
    re_path(r'create/?', views.BookingCreateView.as_view()),
    re_path(r'cancel/<int:pk>/?', views.BookingCancelView.as_view()),
    re_path(r'close/<int:pk>/?', views.BookingCloseView.as_view()),
    # path('/of-restaurant/', views.BookingsOfRestaurant.as_view()),
    re_path(r'history/of-restaurant/<int:pk>/?', views.BookingsHistoryOfRestaurant.as_view()),
]
