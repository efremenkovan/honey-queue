import datetime

from django.db import models
from restaurants.models import Restaurant
from restaurants.serializers import RestaurantSerializer
from tables.models import Table


# Create your models here.
class Booking(models.Model):
    restaurant_id = models.ForeignKey(Restaurant, on_delete = models.CASCADE)
    table_id = models.ForeignKey(Table, on_delete=models.CASCADE, null=True, blank = True)
    phone_number = models.CharField(max_length=11)
    name = models.CharField(max_length=255)
    person_amount = models.IntegerField()
    created_at = models.BigIntegerField(default = round(datetime.datetime.now().timestamp() * 1000))
    date_time = models.BigIntegerField()

    @staticmethod
    def create_booking(
        restaurant_id: int,
        table_id: int,
        phone_number: str,
        name: str,
        person_amount: int,
        created_at: int,
        date_time: int,
    ):
        booking = Booking(
            restaurant_id,
            table_id,
            phone_number,
            name,
            person_amount,
            created_at,
            date_time,
        )
        booking.save()
        return booking
    
    @staticmethod
    def get_for_table(table_id: int):
        return Booking.object.filter(table_id = table_id)
        
    @staticmethod
    def get_restaurant_queue(id: int, include_table_bookings: bool = False):
        if include_table_bookings == False:
            return Booking.objects.filter(restaurant_id = id, table_id = None)
        else:
            return Booking.objects.filter(restaurant_id = id)

    @staticmethod
    def get_table_queue(id: int, first: int = 0):
        if first <= 0:
            return Booking.objects.filter(table_id = id)
        else:
            return Booking.objects.filter(table_id = id)[0:first]

    def close(self):
        bh = BookingHistory(
            restaurant_id=self.restaurant_id,
            table_id=self.table_id,
            phone_number=self.phone_number,
            name=self.name,
            person_amount=self.person_amount,
            created_at=self.created_at,
            date_time=self.date_time,
            is_canceled=False,
        )
        Booking.objects.delete(id = self.id)

    def cancel(self):
        bh = BookingHistory(
            restaurant_id = self.restaurant_id,
            table_id = self.table_id,
            phone_number = self.phone_number,
            name = self.name,
            person_amount = self.person_amount,
            created_at = self.created_at,
            date_time = self.date_time,
            is_canceled = True,
        )
        bh.save()
        self.delete()


class BookingHistory(models.Model):
    restaurant_id = models.ForeignKey(Restaurant, on_delete = models.CASCADE)
    table_id = models.ForeignKey(Table, on_delete=models.CASCADE, null=True, blank = True, default = None)
    phone_number = models.CharField(max_length=11)
    name = models.CharField(max_length=255)
    person_amount = models.IntegerField()
    created_at = models.BigIntegerField()
    date_time = models.BigIntegerField()
    is_canceled = models.BooleanField()

    @staticmethod
    def add_record(
        restaurant_id: int,
        table_id: int,
        phone_number: str,
        name: str,
        person_amount: int,
        created_at: int,
        date_time: int,
        is_canceled: bool = False,
    ):
        record = BookingHistory(
            restaurant_id,
            table_id,
            phone_number,
            name,
            person_amount,
            created_at,
            date_time,
            is_canceled,
        )
        record.save()
        return record

    @staticmethod
    def get_for_table(table_id: int):
        return BookingHistory.objects.filter(table_id=table_id)
        
    @staticmethod
    def get_for_restaurant(restaurant_id: int):
        return BookingHistory.objects.filter(restaurant_id = restaurant_id)

