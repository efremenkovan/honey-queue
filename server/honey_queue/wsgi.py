"""
WSGI config for honey_queue project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.1/howto/deployment/wsgi/
"""

import os
from dotenv import load_dotenv

from django.core.wsgi import get_wsgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'honey_queue.settings')

application = get_wsgi_application()
dir_path = os.path.dirname(os.path.realpath(__file__))
project_folder = os.path.join(dir_path, '..')
load_dotenv(os.path.join(project_folder, './.env'))
