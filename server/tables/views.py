import datetime

from django.http import Http404
from rest_framework import generics, status
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated, IsAuthenticatedOrReadOnly

from bookings.models import Booking
from bookings.serializers import EXPECTED_BOOKING_TIME_DIFF, BookingSerializer
from restaurants.models import Restaurant
from restaurants.serializers import RestaurantSerializer

from . import serializers
from .models import Table


class RestaurantTablesView(generics.ListAPIView):
    serializer_class = serializers.TableSerializer
    queryset = Table.objects.all()

    def get(self, request, pk):
        tables = Table.objects.filter(restaurant_id = pk)
        tables = [serializers.TableSerializer(table).data for table in tables]

        current_time = round(datetime.datetime.now().timestamp() * 1000) if request.GET.get('around') == None else int(request.GET.get('around'))
        time_range = (current_time - EXPECTED_BOOKING_TIME_DIFF, current_time + EXPECTED_BOOKING_TIME_DIFF)

        bookings_in_this_time_range = Booking.objects.filter(
            date_time__gt=time_range[0],
            date_time__lt=time_range[1],
            restaurant_id=pk,
        )
        for table in tables:
            is_taken = False
            for booking in bookings_in_this_time_range:
                if BookingSerializer(booking).data.get('table_id') == table.get('id'):
                    is_taken = True
                    break
            table['is_taken'] = table['is_taken'] or is_taken

        return Response({ 'tables':  tables })


class RestaurantCreateTableView(generics.CreateAPIView):
    serializer_class = serializers.TableSerializer
    permission_classes = [IsAuthenticated]


class RestaurantUpdateTableView(generics.UpdateAPIView):
    serializer_class = serializers.TableSerializer
    permission_classes = [IsAuthenticated]
    queryset = Table.objects.all()


class RestaurantDestroyTableView(generics.DestroyAPIView):
    serializer_class = serializers.TableSerializer
    permission_classes = [IsAuthenticated]
    queryset = Table.objects.all()

    def delete(self, request, pk):
        try:
            table = self.queryset.get(id = pk)
        except Table.DoesNotExist:
            raise Http404

        table_data = serializers.TableSerializer(table).data
        restaurant = Restaurant.objects.get(id = table_data.get("restaurant_id"))
        restaurant_data = RestaurantSerializer(restaurant).data
        if request.user.id != restaurant_data.get("owner"):
            return Response({'content': 'Forbidden'}, status=status.HTTP_403_FORBIDDEN)

        table.delete()
        return Response({}, status=status.HTTP_204_NO_CONTENT)
