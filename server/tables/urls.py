from django.urls import path, re_path

from . import views


urlpatterns = [
    re_path(r'of-restaurant/<int:pk>/?', views.RestaurantTablesView.as_view()),
    path('', views.RestaurantCreateTableView.as_view()),
    re_path(r'<int:pk>/?', views.RestaurantDestroyTableView.as_view()),
    re_path(r'<int:pk>/update/?', views.RestaurantUpdateTableView.as_view()),
]