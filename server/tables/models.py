from django.db import models
from restaurants.models import Restaurant


# Create your models here.
class Table(models.Model):
    restaurant_id = models.ForeignKey(Restaurant, on_delete=models.CASCADE)
    max_people = models.IntegerField(default = 2)
    number = models.IntegerField(unique = True)
    is_taken = models.BooleanField(default = False)

    @staticmethod
    def create_table(restaurant_id: str, max_people: int):
        table = Table(restaurant_id, max_people)
        table.save()
        return table

    @staticmethod
    def remove_table(id):
        Table.objects.delete(id=id)
    
    @staticmethod
    def patch_table(id: str, max_people: int):
        table = Table.objects.get(id=id)
        table.max_people = max_people
        table.save()
        return table
