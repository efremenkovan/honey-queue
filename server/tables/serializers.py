import datetime

from django.core.exceptions import PermissionDenied
from rest_framework import serializers, status
from rest_framework.response import Response

from restaurants.serializers import RestaurantSerializer

from .models import Table


class TableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Table
        fields = '__all__'

    def create(self, validated_data):
        request = self.context.get("request")
        restaurant_owner_id = RestaurantSerializer(validated_data.get("restaurant_id")).data.get("owner")
        if request and hasattr(request, 'user') and request.user.id == restaurant_owner_id:
            table = Table(**validated_data)
            table.save()
            return table
        else:
            raise PermissionDenied({'content': 'Forbidden'})

    def update(self, instance, validated_data):
        request = self.context.get("request")
        restaurant_owner_id = RestaurantSerializer(instance.restaurant_id).data.get("owner")

        if request and hasattr(request, 'user') and request.user.id == restaurant_owner_id:
            instance.max_people = validated_data.get('max_people', instance.max_people)
            instance.number = validated_data.get('number', instance.number)
            instance.is_taken = validated_data.get('is_taken', instance.is_taken)
            instance.save()
            return instance
        else:
            raise PermissionDenied({'content': 'Forbidden'})
